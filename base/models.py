from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from django.conf import settings

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_admin = models.IntegerField(default=0)
    is_requester = models.IntegerField(default=0)
    is_worker = models.IntegerField(default=1)
    confirmation_code = models.TextField(blank=True)
class SentenceTextData(models.Model):
    user = models.ForeignKey(User, null=True, default=None, blank=True)
    text_message = models.CharField(max_length=255, blank=True)
    json_name = models.CharField(max_length=255, blank=True)
    created = models.DateTimeField(editable=False, auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    is_published = models.IntegerField(default=0)