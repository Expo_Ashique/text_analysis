from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from base.models import *
import uuid
from django.db import IntegrityError
import smtplib
from email.mime.text import MIMEText
from django.contrib.auth import authenticate
from django.core.files.storage import FileSystemStorage
import json
from base.models import *
from django.shortcuts import render_to_response
from django.template import RequestContext
from twitter import *
import datetime
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import tweepy
from tweepy import OAuthHandler

#Tweepy authintication

consumer_key = 'LglekCkggFHeAKwaAZ0webw4d'
consumer_secret = 'LoIRSZ2EiIzFiwvlFQv7FyYPBecFwchCzAnnusfuj8Pq0OAnju'
access_token_key = '923076988549529600-3qU3190l2W7lbrGt4VC6UstEKa5zdu6'
access_token_secret = 'rwNWjG2QdG0r7ny4B2C6t4jt8RmbGS6vRaAVxGUnVlycN'
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token_key, access_token_secret)
# Create your views here.
def home(request):
    return render(request, 'base/home.html')


def registration(request):
    # registration module
    if request.user.is_authenticated():
        return HttpResponseRedirect('/base/home/')

    elif request.method == "POST":
        user_username = request.POST['username']
        user_password = request.POST['password']
        user_email = request.POST['email']
        user_name = request.POST['name']

        # checking existing email.
        existing_email = User.objects.filter(email=user_email).first()
        if existing_email is not None:
            message = '"' + user_email + '" already exist. Please try with a different email.'

            return render(request, 'base/basic/registration.html',
                          {'error_message': message})

        try:
            user = User.objects.create_user(username=user_username, first_name=user_name, email=user_email)
        except IntegrityError as e:

            e.message = '"' + user_username + '" already exist. Please try with a different username.'

            return render(request, 'base/basic/registration.html',
                          {'error_message': e.message})

        user.set_password(raw_password=user_password)

        user.is_staff = True
        user.is_superuser = False
        user.is_active = False
        user.save()

        confirmation_code = str(uuid.uuid4())

        profile = Profile(user_id=user.pk, is_worker=1, confirmation_code=confirmation_code)
        profile.save()

        # START confirmation email
        target_url = "http://157.7.242.91/base/confirm-email/" + confirmation_code

        sender = 'noreply@codenext.com'
        recipient = user_email
        message = '<p>Dear Mr. ' + user_name + ',</p><p>You have successfully registered. Please <a href="' + target_url + '">click here</a> to confirm your registration.</p><p>Thanks,</p><p>Team CodeNext</p>'

        msg = MIMEText(message, 'html')
        msg['Subject'] = "CodeNext: Registration"
        msg['From'] = sender
        msg['To'] = recipient

        server = smtplib.SMTP_SSL(settings.EMAIL_HOST, settings.EMAIL_PORT)
        server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        server.sendmail(sender, [recipient], msg.as_string())
        server.quit()
        # END confirmation email


        return render(request, 'base/basic/registration.html',
                      {'success_message': 'Account activation link has been sent to you email address. Please check.'})
    else:
        return render(request, 'base/basic/registration.html')
def confirm_email(request, key):
    try:
        profile = Profile.objects.get(confirmation_code = key)
        User.objects.filter(id=profile.user_id).update(is_active = 1)

        # update previous confirmation code to prevent activate later if admin block this user
        profile = Profile.objects.filter(confirmation_code = key).update(confirmation_code = '')

        message = "You account has been activate"
        return render(request, 'base/basic/confirm_email.html',{ 'success_message': message })

    except:
        message = "Link expired"
        return render(request, 'base/basic/confirm_email.html',{ 'error_message': message })


def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/base/home/')

    else:

        from django.contrib.auth import login
        loginStatus = request.user.is_authenticated

        user = None
        print(loginStatus)

        if request.method == 'POST':
            # uname = request.POST['username']

            uemail = request.POST['email']
            pword = request.POST['password']
            LoginRequestUser = User.objects.filter(email=uemail).first()
            user = authenticate(username=LoginRequestUser.username, password=pword)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    # return HttpResponseRedirect('/base/begin/')
                    return HttpResponseRedirect('/base/home/')
                else:
                    return render(request, 'base/basic/login.html', {'message': "Its not active user"})
            else:
                return render(request, 'base/basic/login.html',
                              {'message': "Invalid login or your account is inactive"})

        elif request.method == 'GET':
            if (loginStatus == True):
                return HttpResponseRedirect('/base/home/')
            else:
                return render(request, 'base/basic/login.html', {})
    return render(request, 'base/basic/login.html', {'message': "Invalid User name Or Password"})

def logout(request):
    if request.user.is_authenticated():
        from django.contrib.auth import logout
        logout(request)
        return HttpResponseRedirect('/base/login')
    else:
        return HttpResponseRedirect('/base/login')




def sentence_text_analysis(request):
        user_id = request.user.id
        if request.method == 'POST':
            # upload json file

            myfile = request.FILES['json']
            fs = FileSystemStorage(settings.MEDIA_ROOT + '/temp/')
            json_filename = fs.save(myfile.name, myfile)

            fo = open(settings.MEDIA_ROOT + '/temp/' + json_filename,"r")
            file_data= fo.read()
            fo.close()

            json_data = json.loads(json.dumps(file_data))

            decoded = json.loads('{"sns_text_analysis": ' + json_data + '}')

            for item in  decoded["sns_text_analysis"]:
                links = item["links"]
                nodes = item["nodes"]

            text_message = ""
            sentenceTextData = SentenceTextData()
            sentenceTextData.user_id = user_id
            sentenceTextData.text_message = request.POST['textMessage']
            text_message = sentenceTextData.text_message
            sentenceTextData.json_name = json_filename

            # sentenceTextData.save()




            # print(json.dumps(nodes))


            return render(request, 'base/sentence_text_analysis/display_sentence_text_analysis_result.html',
                          {'text_message': text_message,'group':json.dumps(nodes),'source':json.dumps(links)})

        else:
            return render(request, 'base/sentence_text_analysis/sentence_text_analysis_form.html')

def aggregate_related_tweets(request):
    if request.method == 'POST':
        search_word = request.POST['search_word']
        results = []

        api = tweepy.API(auth_handler=auth)
        latest=api.search(q=search_word,since_id='1', count=100)


        for fr in latest:
            status = fr.text
            tweet_date = fr.created_at
            lastId=fr.id
            results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})

        latest_2=api.search(q=search_word, since_id='1',count=100,max_id=lastId)
        for fr in latest_2:
            status = fr.text
            tweet_date = fr.created_at
            lastId = fr.id
            results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
        latest_3=api.search(q=search_word,since_id='1', count=100,max_id=lastId)
        for fr in latest_3:
            status = fr.text
            tweet_date = fr.created_at
            lastId = fr.id
            results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
        latest_4 = api.search(q=search_word,since_id='1', count=100, max_id=lastId)
        for fr in latest_4:
            status = fr.text
            tweet_date = fr.created_at
            lastId = fr.id
            results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
        latest_5 = api.search(q=search_word,since_id='1', count=100, max_id=lastId)
        for fr in latest_5:
            status = fr.text
            tweet_date = fr.created_at
            results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
        # print(len(results))
        return render(request, 'base/aggregate_related_tweets/aggregate_tweets.html', {'results': results})
    else:
        return render(request, 'base/aggregate_related_tweets/aggregate_tweets.html', {})



def download_tweets(request):
    try:
        import csv
        new_results = []
        results = request.POST.get('results')
        numberOfFiles = request.POST.get('numberOfFiles')

        decoded = json.loads('{"tweetLists": ' + results + '}')

        file_name = "Tweets.csv"
        dest_url = settings.MEDIA_ROOT + '/' + file_name
        fx = open(dest_url, "w")

        fx.write("status\n")
        count = 0;
        for item in decoded["tweetLists"]:
            if (count < int(numberOfFiles)):
                status = item["status"]
                tweet_date = item["tweet_date"]
                fx.write(status + "," + tweet_date + "\n")
                count += 1
        fx.close()
        return HttpResponse(file_name)

    except:
        return HttpResponse('0')


    # response = HttpResponse()
    # response['Content-Disposition'] = 'attachment; filename=' + dest_url
    # return response



def aggregate_user_tweets(request):
    results=[]
    if request.method == 'GET':
        search_word = request.GET.get('search_word')
        # search_word = request.GET['search_word']
        if search_word is not None:

            try:


                api=tweepy.API(auth_handler=auth)

                latest = api.user_timeline(screen_name=search_word,count=200)

                for fr in latest:
                    status = fr.text
                    tweet_date = fr.created_at
                    lastId = fr.id
                    results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
                latest_2 = api.user_timeline(screen_name=search_word, since_id='1', count=200, max_id=lastId)
                for fr in latest_2:
                    status = fr.text
                    tweet_date = fr.created_at
                    lastId = fr.id
                    results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
                latest_3 = api.user_timeline(screen_name=search_word, since_id='1', count=200, max_id=lastId)
                for fr in latest_3:
                    status = fr.text
                    tweet_date = fr.created_at
                    lastId = fr.id
                    results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
                latest_4 = api.user_timeline(screen_name=search_word, since_id='1', count=200, max_id=lastId)
                for fr in latest_4:
                    status = fr.text
                    tweet_date = fr.created_at
                    lastId = fr.id
                    results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
                latest_5 = api.user_timeline(screen_name=search_word, since_id='1', count=200, max_id=lastId)
                for fr in latest_5:
                    status = fr.text
                    tweet_date = fr.created_at
                    results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
                # print(len(results))
                page = request.GET.get('page', 1)

                paginator = Paginator(results, 20)
                try:
                    new_results = paginator.page(page)
                except PageNotAnInteger:
                    new_results = paginator.page(1)
                except EmptyPage:
                    new_results = paginator.page(paginator.num_pages)

                return render(request, 'base/aggregate_user_tweets/aggregate_user_tweets.html', {'results': new_results, "search_word": search_word})


            except:
                results.append({'status': 'Follow us @CodeNext.com', 'date': 'about 10 minutes ago'})
        else:
            return render(request, 'base/aggregate_user_tweets/aggregate_user_tweets.html')
def download_User_tweets(request):
    try:
        numberOfFiles = request.POST.get('numberOfFiles')
        search_word = request.POST.get('search_word')

        api = tweepy.API(auth_handler=auth)

        latest = api.user_timeline(screen_name=search_word, count=200)

        results = []
        for fr in latest:
            status = fr.text
            tweet_date = fr.created_at
            lastId = fr.id

            results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
        latest_2 = api.user_timeline(screen_name=search_word, count=200, max_id=lastId)
        for fr in latest_2:
            status = fr.text
            tweet_date = fr.created_at
            lastId = fr.id
            results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})

        latest_3 = api.user_timeline(screen_name=search_word,  count=200, max_id=lastId)
        for fr in latest_3:
            status = fr.text
            tweet_date = fr.created_at
            lastId = fr.id
            results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
        latest_4 = api.user_timeline(screen_name=search_word, count=200, max_id=lastId)
        for fr in latest_4:
            status = fr.text
            tweet_date = fr.created_at
            lastId = fr.id
            results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
        latest_5 = api.user_timeline(screen_name=search_word,  count=200, max_id=lastId)
        for fr in latest_5:
            status = fr.text
            tweet_date = fr.created_at
            results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})
        # print(len(results))
        decoded = json.loads('{"tweetLists": ' + json.dumps(results) + '}')

        file_name = "Tweets.csv"
        dest_url = settings.MEDIA_ROOT + '/' + file_name
        fx = open(dest_url, "w")

        fx.write("status\n")
        count = 0;
        for item in decoded["tweetLists"]:
            if (count < int(numberOfFiles)):
                status = item["status"]
                tweet_date = item["tweet_date"]
                fx.write(status + "," + tweet_date + "\n")
                count += 1
        fx.close()
        return HttpResponse(file_name)
    except:
        return HttpResponse('0')


def tweeter_user_text_analysis(request):
    return render(request, 'base/tweeter_user_text_analysis/tweeter_user_text_analysis.html')

def tweeter_user_text_analysis_display(request):
    results=[]
    userName = request.POST.get('message')
    api = tweepy.API(auth_handler=auth)
    try:
        latest = api.user_timeline(screen_name=userName, count=200)
        user = api.get_user(screen_name=userName)
        # print(user)
        for fr in latest:
            status = fr.text
            tweet_date = fr.created_at
            results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})

    except:
        user=None
        results.append({'status': "Not an Authorized User", 'tweet_date': ''})
    return render(request, 'base/tweeter_user_text_analysis/tweeter_user_text_analysis_display.html',
                  {'results': results, 'user': user})

def tweeter_word_text_analysis(request):
    return render(request, 'base/tweeter_word_text_analysis/tweeter_word_text_analysis.html')

def tweeter_word_text_analysis_display(request):
    results = []
    searchWord = request.POST.get('message')
    api = tweepy.API(auth_handler=auth)
    latest = api.search(q=searchWord, since_id='1', count=100)

    for fr in latest:
        status = fr.text
        tweet_date = fr.created_at
        # lastId = fr.id
        user_screen_name = fr.user.screen_name
        user_name=fr.user.name
        time_zone=fr.user.time_zone
        profile_image_url_https=fr.user.profile_image_url_https
        results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S'),
                        'user_screen_name':user_screen_name,'user_name':user_name,'time_zone':time_zone,'profile_image_url_https':profile_image_url_https})



    # latest_2 = api.search(q=searchWord, since_id='1', count=100, max_id=lastId)
    # for fr in latest_2:
    #     status = fr.text
    #     tweet_date = fr.created_at
    #     results.append({'status': status, 'tweet_date': tweet_date.strftime('%d-%m-%Y %H:%M:%S')})

    return render(request, 'base/tweeter_word_text_analysis/tweeter_word_text_analysis_display.html',
                  {'results':results,'searchWord':searchWord})
