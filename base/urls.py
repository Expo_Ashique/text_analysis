from django.conf.urls import url
from django.contrib import admin
from . import views
from django.conf.urls.static import static
from django.conf import settings
urlpatterns = [
    url(r'^home/$', views.home, name="home"),
    url(r'^registration/$', views.registration, name="registration_view"),
    url(r'^confirm-email/(?P<key>.*)/$', views.confirm_email, name="confirm_email"),
    url(r'^login/$', views.login, name="login_view"),
    url(r'^logout/$', views.logout, name="logout_view"),
    url(r'^sentence_text_analysis/$', views.sentence_text_analysis, name="sentence_text_analysis"),
    url(r'^aggregate_related_tweets/$', views.aggregate_related_tweets, name="aggregate_related_tweets"),
    url(r'^download_tweets/$', views.download_tweets, name="download_tweets"),
    url(r'^aggregate_user_tweets/$', views.aggregate_user_tweets, name="aggregate_user_tweets"),
    url(r'^download_User_tweets/$', views.download_User_tweets, name="download_User_tweets"),
    url(r'^tweeter_user_text_analysis/$', views.tweeter_user_text_analysis, name="tweeter_user_text_analysis"),
    url(r'^tweeter_user_text_analysis_display/$', views.tweeter_user_text_analysis_display, name="tweeter_user_text_analysis_display"),
    url(r'^tweeter_word_text_analysis/$', views.tweeter_word_text_analysis, name="tweeter_word_text_analysis"),
    url(r'^tweeter_word_text_analysis_display/$', views.tweeter_word_text_analysis_display,
        name="tweeter_word_text_analysis_display"),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)